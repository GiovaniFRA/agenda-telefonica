#include <stdio.h>
#include <string.h>

struct Contatos{
	char nome[100]; 
	char sobrenome[100]; 
	long telefone;
	char email[100];
};

void inicializar_contatos(struct Contatos *ctt){
	for(int i=0;i<10;i++){
		strcpy(ctt[i].nome,"");
		strcpy(ctt[i].sobrenome,"");
		ctt[i].telefone=0;
		strcpy(ctt[i].email,"");
	}
}

void cria_3_contatos(struct Contatos *ctt){
	
	strcpy(ctt[0].nome,"Roger");
	strcpy(ctt[0].sobrenome,"Pinto");
	ctt[0].telefone=98218996;
	strcpy(ctt[0].email,"roger@gmail.com");
	
	strcpy(ctt[1].nome,"Wiliam");
	strcpy(ctt[1].sobrenome,"Pinto");
	ctt[1].telefone=98218996;
	strcpy(ctt[1].email,"roger@gmail.com");
	
	strcpy(ctt[2].nome,"Jao");
	strcpy(ctt[2].sobrenome,"Pinto");
	ctt[2].telefone=98218996;
	strcpy(ctt[2].email,"roger@gmail.com");

}

int procurar_posicao_vazia(struct Contatos ctt[10]){
	int i=0;
	while(strcmp(ctt[i].nome,"")){
		i++;
	}
	return i;
}

void adicionar(struct Contatos *ctt){
   int index=procurar_posicao_vazia(ctt);
   printf("%d",index);
   if (index<10)
   {
		printf("digite o nome: ");
		scanf("%s",&ctt[index].nome);

		printf("digite o sobrenome: ");
		scanf("%s",&ctt[index].sobrenome);

		printf("digite o numero: ");
		scanf("%d",&ctt[index].telefone);

		printf("digite o email: ");
		scanf("%s",&ctt[index].email);
   }else{
	 puts("Nao ha mais espacos vazios\n tente deletar algum contato");
   }
   
}

void remover(struct Contatos *ctt){
	int i=0;
	listar_pessoas(ctt,3);
	puts("\ndigite o numero do contato a ser removido: ");
	scanf("%d",&i);
	i=i-1;
	strcpy(ctt[i].nome,"");
	strcpy(ctt[i].sobrenome,"");
	ctt[i].telefone=0;
	strcpy(ctt[i].email,"");
}

void listar_um(struct Contatos x, int um){
   	if(um == -1){	
		printf("\n%s ",x.nome);
	}else if(um==0){
		printf(" %s ",x.nome);
		printf(" %s ",x.sobrenome);
   		printf(" %d ",x.telefone);
   		printf(" %s\n ",x.email);
	}else if(um==2){
		printf("1 nome: %s ",x.nome);
		printf("\n2 Sobrenome: %s ",x.sobrenome);
   		printf("\n3 Telefone %d ",x.telefone);
   		printf("\n4 Email: %s\n ",x.email);
	}else if(um==3){
		printf("%s \n",x.nome);
	}
}

void listar_pessoas(struct Contatos x[10],int detalhes){
	system("clear");
   for(int i=0;i<10;i++){
	   if(detalhes==3){printf("%d ",i+1);}
	  if(x[i].nome!=""){
			 listar_um(x[i],detalhes);
	  }
   }
}

void editar_contato(struct Contatos x[10]){
	int pessoa;
	int item;
	listar_pessoas(x,3);
	puts("\nescolha um contato para editar: \n");
	scanf("%d",&pessoa);
	system("clear");
	listar_um(x[pessoa-1],2);
	puts("\n escolha um item para alterar ");
	altera_item(pessoa,x,item);
}

void altera_item(int pessoa,struct Contatos *x,int i){
	char alteracao[100];
	scanf("%d",&i);
	switch(i){
		case 1: puts("escreva o nome desejado\n");
				scanf("%s",&alteracao);
				strcpy(x[pessoa-1].nome,alteracao);
		break;
		case 2: puts("escreva o sobrenome desejado\n");
				scanf("%s",&alteracao);
				strcpy(x[pessoa-1].sobrenome,alteracao);
		break;
		case 3: puts("escreva o telefone desejado\n");
				scanf("%d",&i);
				x[pessoa-1].telefone=i;
		break;
		case 4: puts("escreva o email desejado\n");
				scanf("%s",&alteracao);
				strcpy(x[pessoa-1].email,alteracao);
		break;
	
	}
}	

void menu(){
	system("clear");
	printf("1-Listar  2-listar com detalhes   3-Adicionar\n4-Remover  5-Editar   0-Sair\n");
}

int main(){
	struct Contatos ctt[10];
	int opcao=-1;
	inicializar_contatos(ctt);
	cria_3_contatos(ctt);
	//listar_pessoas(ctt,1);
	
	while (opcao!=0){
		menu();
		scanf("%d",&opcao);
		switch (opcao){
			case 1:
					listar_pessoas(ctt,-1);
					puts("continuar? 1-sim 0-nao");
					scanf("%d",&opcao);
					system("clear");
			break;

			case 2:
					listar_pessoas(ctt,0);
					puts("continuar? 1-sim 0-nao");
					scanf("%d",&opcao);
					system("clear");
			break;

			case 3:
					adicionar(ctt);
					puts("continuar? 1-sim 0-nao");
					scanf("%d",&opcao);
					system("clear");
			break;

			case 4:
					remover(ctt);
					puts("continuar? 1-sim 0-nao");
					scanf("%d",&opcao);
					system("clear");
			break;

			case 5:
					editar_contato(ctt);
					puts("continuar? 1-sim 0-nao");
					scanf("%d",&opcao);
					system("clear");
			break;
			case 0:
				puts("fim da execucao");
			break;
			default:	
					puts("escolha uma opcao valida");
			break;
		}
	}
	
	
}
